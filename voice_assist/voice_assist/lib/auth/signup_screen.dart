// ignore_for_file: depend_on_referenced_packages

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:voice_assist/auth/login_screen.dart';
import 'package:voice_assist/constants/colors.dart';

class Signup extends StatefulWidget {
  const Signup({super.key});

  @override
  State<Signup> createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool obscureText = true;

  Future<void> _handleSignup() async {
    final String name = nameController.text;
    final String email = emailController.text;
    final String password = passwordController.text;


    // Check if any field is empty
    if (name.isEmpty || email.isEmpty || password.isEmpty) {
      _handleSignupError("Please fill in all fields");
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Please fill in all fields'),
      ));
      return; 
    }

    const String apiUrl = 'http://192.168.100.130:3000/auth/signup';

    try {
      final http.Response response = await http.post(
        Uri.parse(apiUrl),
        body: {'name': name, 'email': email, 'password': password},
      );

      if (response.statusCode == 201) {
        _handleSignupSuccess(response);
      } else {
        _handleSignupFailure(response);
      }
    } catch (e) {
      _handleSignupError(e);
    }
  }

  void _handleSignupSuccess(http.Response response) {
    final Map<String, dynamic> responseData = json.decode(response.body);
    final String token = responseData['token'];
    // ignore: avoid_print
    print('Signup successful. Token: $token');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => const LoginScreen()),
    );
  }

  void _handleSignupFailure(http.Response response) {
    // final int statusCode = response.statusCode;

    try {
      final Map<String, dynamic> responseData = json.decode(response.body);
      final String errorMessage = responseData['message'] ?? 'Unknown error';
      // print('Signup failed with status code: $statusCode - $errorMessage');
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Signup failed  $errorMessage'),
      ));
    } catch (e) {
      // print('Error decoding response body: $e');
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Signup failed with status code Unknown error'),
      ));
    }
  }

  void _handleSignupError(dynamic error) {
    // print('Error: $error');
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Scaffold(
        body: Container(
          decoration: const BoxDecoration(gradient: appGradient),
          child: Padding(
            padding: const EdgeInsets.all(22.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Signup',
                  style: TextStyle(fontSize: 40, color: Colors.white),
                ),
                const SizedBox(
                  height: 20,
                ),

                // Name textfield
                const Text(
                  'Name:',
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF64C3B2),
                  ),
                ),
                const SizedBox(height: 8.0),
                TextField(
                  controller: nameController,
                  decoration: const InputDecoration(
                    labelText: 'Enter your name',
                    labelStyle: TextStyle(color: Colors.white),
                    border: OutlineInputBorder(),
                    suffixIcon: Padding(
                      padding: EdgeInsets.only(right: 20),
                      child: Icon(
                        Icons.person,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  style: const TextStyle(color: Colors.white),
                ),
                const SizedBox(height: 16.0),

                // Email textfield
                const Text(
                  'Email',
                  style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF64C3B2)),
                ),
                const SizedBox(height: 8.0),
                TextField(
                  controller: emailController,
                  decoration: const InputDecoration(
                    labelText: 'Enter your email',
                    labelStyle: TextStyle(color: Colors.white),
                    border: OutlineInputBorder(),
                    suffixIcon: Padding(
                      padding: EdgeInsets.only(right: 20),
                      child: Icon(
                        Icons.email_sharp,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  style: const TextStyle(color: Colors.white),
                ),
                const SizedBox(height: 16.0),

                // Password textfield
                const Text(
                  'Password:',
                  style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF64C3B2)),
                ),
                const SizedBox(height: 8.0),
                TextField(
                  controller: passwordController,
                  decoration: InputDecoration(
                    labelText: 'Enter your password',
                    labelStyle: const TextStyle(color: Colors.white),
                    border: const OutlineInputBorder(),
                    suffixIcon: Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: IconButton(
                        icon: Icon(
                          obscureText ? Icons.visibility : Icons.visibility_off,
                          color: Colors.grey,
                        ),
                        onPressed: () {
                          setState(() {
                            obscureText = !obscureText;
                          });
                        },
                      ),
                    ),
                  ),
                  style: const TextStyle(color: Colors.white),
                  obscureText: obscureText,
                ),
                const SizedBox(height: 16.0),

                // Signup button
                Container(
                  width: 400,
                  height: 50,
                  color: const Color(0xFF64C3B2),
                  child: MaterialButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0),
                    ),
                    onPressed: _handleSignup,
                    child: const Text('Signup'),
                  ),
                ),
                const SizedBox(height: 5),
                const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Or',
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      "Already have an account?",
                      style: TextStyle(fontSize: 12, color: Colors.white),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const LoginScreen()),
                        );
                      },
                      child: const Text(
                        'Login',
                        style:
                            TextStyle(color: Color(0xFF64C3B2), fontSize: 15),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
