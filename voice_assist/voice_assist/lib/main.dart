import 'package:flutter/material.dart';
import 'package:voice_assist/splash_screen.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Color(0xFF191B28), Color(0xFF191B28)],
            ),
          ),
          child: const Center(
            child: SplashScreen(),
          ),
        ),
      ),
    );
  }
}
