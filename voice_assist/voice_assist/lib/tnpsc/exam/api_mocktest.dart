import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class QuestionFetcher extends StatefulWidget {
  final String context;
  final String prompt;
  QuestionFetcher({required this.context, required this.prompt});
  @override
  _QuestionFetcherState createState() => _QuestionFetcherState();
}

class _QuestionFetcherState extends State<QuestionFetcher> {
  List<Map<String, dynamic>>? questions;
  bool isLoading = false;
  bool isError = false;
  late String errorMessage;
  int questionIndex = 0;
  int score = 0;

  @override
  void initState() {
    super.initState();
    fetchQuestions();
  }

  Future<void> fetchQuestions() async {
    setState(() {
      isLoading = true;
      isError = false;
    });

    final Map<String, dynamic> requestBody = {
      'context': widget.context,
      'prompt': widget.prompt,
    };

    final Uri url =
        Uri.parse('http://192.168.100.130:2000/openai/chat-completion');

    try {
      final response = await http.post(
        url,
        body: jsonEncode(requestBody),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      );
      setState(() {
        questions = List<Map<String, dynamic>>.from(jsonDecode(response.body));
        isLoading = false;
      });
      print(questions);
    } catch (e) {
      setState(() {
        isLoading = false;
        isError = true;
        errorMessage = 'Error fetching questions: $e';
      });
    }
  }

  void nextQuestion() {
    setState(() {
      questionIndex++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF191B28),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            automaticallyImplyLeading: false,
            expandedHeight: 130.0,
            flexibleSpace: FlexibleSpaceBar(
              title: const Text(
                'TNPSC Group 1',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              titlePadding: const EdgeInsetsDirectional.only(start: 20, bottom: 16),
              background: Container(
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color(0xFF38BBD5),
                      Color(0xFF38BDCB),
                      Color(0xFF6EF5B5),
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: _buildBody(),
          ),
        ],
      ),
    );
  }

  Widget _buildBody() {
    if (isLoading) {
      return const Center(
        child: Padding(
          padding: EdgeInsets.only(top: 350),
          child: SizedBox(
            width: 50,
            height: 50,
            child: CircularProgressIndicator(
              strokeWidth: 2,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
            ),
          ),
        ),
      );
    } else if (isError) {
      return Center(child: Text(errorMessage));
    } else if (questions == null || questions!.isEmpty) {
      return const Center(child: Text('No questions available'));
    } else if (questionIndex >= questions!.length) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Padding(
              padding: EdgeInsets.only(top: 340),
              child: Text(
                'Quiz Completed!',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
            Text(
              'Your Score: $score',
              style: const TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );
    } else {
      return _buildQuestionCard();
    }
  }

  Widget _buildQuestionCard() {
    int? selectedAnswerIndex;
    String correctAnswer = '';
    return Container(
      child: Card(
        color: const Color.fromARGB(255, 18, 19, 29),
        margin: const EdgeInsets.only(top: 150, right: 20, left: 20),
        child: Column(
          children: [
            ListTile(
              title: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  questions![questionIndex]['question'],
                  style: const TextStyle(color: Colors.white, fontSize: 20),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Column(
              children: List.generate(
                questions![questionIndex]['answers'].length,
                (answerIndex) {
                  return Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            selectedAnswerIndex = answerIndex;
                            correctAnswer = questions![questionIndex]['answers']
                                [questions![questionIndex]
                                    ['correctAnswerIndex']];
                            if (answerIndex ==
                                questions![questionIndex]
                                    ['correctAnswerIndex']) {
                              score++;
                            }
                          });
                          if (answerIndex ==
                              questions![questionIndex]['correctAnswerIndex']) {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                backgroundColor: Colors.teal,
                                behavior: SnackBarBehavior.floating,
                                margin: const EdgeInsets.all(50),
                                duration: const Duration(milliseconds: 1800),
                                content: Text('Correct Answer: $correctAnswer'),
                              ),
                            );
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                backgroundColor: Colors.red[900],
                                behavior: SnackBarBehavior.floating,
                                margin: const EdgeInsets.all(50),
                                duration: const Duration(milliseconds: 1800),
                                content: Text(
                                    'InCorrect Answer!, Correct answer is:$correctAnswer'),
                              ),
                            );
                          }
                          Future.delayed(const Duration(seconds: 2), () {
                            nextQuestion();
                          });
                        },
                        child: Container(
                          margin: const EdgeInsets.only(top: 10),
                          padding: const EdgeInsets.only(bottom: 10),
                          child: Column(
                            children: [
                              Container(
                                width: 350,
                                height: 35,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    gradient: LinearGradient(
                                      colors: selectedAnswerIndex == answerIndex
                                          ? [Colors.green, Colors.blue]
                                          : [
                                              const Color.fromARGB(
                                                  255, 225, 229, 230),
                                              const Color.fromARGB(
                                                  255, 229, 240, 241),
                                              const Color.fromARGB(
                                                  255, 229, 233, 231),
                                            ], // No gradient when not selected
                                    )),
                                padding: const EdgeInsets.all(5),
                                margin: const EdgeInsets.symmetric(
                                    vertical: 8, horizontal: 16),
                                child: Center(
                                  child: Text(
                                    questions![questionIndex]['answers']
                                        [answerIndex],
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: selectedAnswerIndex == answerIndex
                                          ? Colors.green
                                          : Colors.black,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      if (selectedAnswerIndex != null)
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Text(
                            'Correct Answer: $correctAnswer',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: selectedAnswerIndex ==
                                      questions![questionIndex]
                                          ['correctAnswerIndex']
                                  ? Colors.green
                                  : Colors.red,
                            ),
                          ),
                        ),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
