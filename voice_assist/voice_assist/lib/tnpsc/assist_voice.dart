import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_sound/flutter_sound.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import 'package:flutter_tts/flutter_tts.dart';

class ChatCompletionPage extends StatefulWidget {
  final String context1;
  ChatCompletionPage({required this.context1});
  @override
  _ChatCompletionPageState createState() => _ChatCompletionPageState();
}

class _ChatCompletionPageState extends State<ChatCompletionPage> {
  FlutterSoundRecorder? _recorder;
  TextEditingController _contextController = TextEditingController();
  String? _audioContent; // Update the type to String?
  stt.SpeechToText _speech = stt.SpeechToText();
  FlutterTts _flutterTts = FlutterTts();

  @override
  void initState() {
    super.initState();
    _recorder = FlutterSoundRecorder();
  }

  Future<void> _startRecording() async {
    try {
      await _recorder!.openRecorder();
      await _recorder!.startRecorder(toFile: 'audio.wav');
    } catch (e) {
      print('Failed to start recording: $e');
    }
  }

  Future<void> _stopRecording() async {
    try {
      String? path = await _recorder!.stopRecorder();
      if (path != null) {
        _submit(File(path));
      } else {
        print('Recording path is null');
      }
    } catch (e) {
      print('Failed to stop recording: $e');
    } finally {
      await _recorder!.closeRecorder();
    }
  }

  Future<void> _submit(File audioFile) async {
    final context = widget.context1;

    String url = 'http://192.168.100.130:2000/openai/chat-completion-to-audio';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.fields['context'] = context;
    request.files
        .add(await http.MultipartFile.fromPath('audioFile', audioFile.path));

    try {
      var streamedResponse = await request.send();
      var response = await http.Response.fromStream(streamedResponse);
      var jsonResponse = json.decode(response.body);
      setState(() {
        _audioContent = jsonResponse['audioContent'];
      });

      // Play the audio response
      await _flutterTts.setVolume(1.0);
      await _flutterTts.speak(_audioContent!);
    } catch (error) {
      print('Error: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Chat Completion to Audio'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ElevatedButton(
              onPressed: () async {
                if (await _speech.initialize()) {
                  await _speech.listen(
                    onResult: (result) {
                      setState(() {
                        _contextController.text = result.recognizedWords;
                      });
                    },
                  );
                } else {
                  print('Speech recognition not available');
                }
              },
              child: const Text('Start Chat'),
            ),
            const SizedBox(height: 20),
            TextField(
              controller: _contextController,
              decoration: const InputDecoration(labelText: 'Chat Context'),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: _startRecording,
              child: const Text('Start Recording'),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: _stopRecording,
              child: const Text('Stop Recording'),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _recorder!.closeRecorder();
    super.dispose();
  }
}
