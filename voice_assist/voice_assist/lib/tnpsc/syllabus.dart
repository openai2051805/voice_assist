import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Syllabus extends StatefulWidget {
  final String prompt;

  const Syllabus({required this.prompt, Key? key}) : super(key: key);

  @override
  State<Syllabus> createState() => _SyllabusState();
}

class _SyllabusState extends State<Syllabus> {
  String generatedText = '';
  bool isLoading = true;
  String? error;

  @override
  void initState() {
    super.initState();
    generateOpenAIResponse();
  }

  Future<void> generateOpenAIResponse() async {
    final url = Uri.parse(
        'https://api.openai.com/v1/engines/gpt-3.5-turbo-instruct/completions');
    final prompt = widget.prompt;

    try {
      final response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Authorization':
              'Bearer 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',
        },
        body: jsonEncode({
          'prompt': prompt,
          'max_tokens': 2000
        }), // Adjust max_tokens as needed
      );

      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        setState(() {
          generatedText = responseData['choices'][0]['text'];
          isLoading = false;
        });
      } else {
        throw Exception('Failed to load response');
      }
    } catch (e) {
      setState(() {
        error = 'Error fetching data: $e';
        isLoading = false;
      });
      print('Error: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: isLoading
          ? const CircularProgressIndicator()
          : error != null
              ? Text(error!)
              : Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [Color(0xFF191B28), Color(0xFF191B28)],
                      ),
                    ),
                    width: 450,
                    height: 970,
                    padding: const EdgeInsets.all(16.0),
                    child: SingleChildScrollView(
                      child: Text(
                        generatedText,
                        style: const TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.normal,
                            color: Colors.white,
                            decoration: TextDecoration.none),
                      ),
                    ),
                  ),
                ),
    );
  }
}
