import 'package:flutter/material.dart';
import 'package:voice_assist/constants/colors.dart';
import 'package:voice_assist/tnpsc/assist_voice.dart';
import 'package:voice_assist/tnpsc/exam/api_mocktest.dart';
import 'package:voice_assist/tnpsc/syllabus.dart';

class Group1home extends StatelessWidget {
  const Group1home({super.key});

  void _tosyllabus(BuildContext context, String prompt) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => Syllabus(prompt: prompt)));
  }

  void _toquizapi(BuildContext context, String prompt, String role) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                QuestionFetcher(prompt: prompt, context: role)));
  }

  // void _tovoice(BuildContext context, String context1) {
  //   Navigator.push(
  //       context,
  //       MaterialPageRoute(
  //           builder: (context) => ChatCompletionPage(context1: context1)));
  // }

  @override
  Widget build(BuildContext context) {
    String prompt = 'list out the TNPSC group1 exam syllabus';
    String prompt1 =
        'Give 10 Tnspc group1 question.Each questions with 4 answers';
    String role =
        'You are a quiz generator. Output JSON format: {questions: [{question: question content, answers: [], correctAnswerIndex: 0}]}.';

    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(gradient: appGradient),
        child: Column(
          children: [
            Container(
              height: 250,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xFF38BBD5),
                    Color(0xFF38BDCB),
                    Color(0xFF6EF5B5),
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [0.0, 0.5, 1.0],
                  tileMode: TileMode.clamp,
                ),
              ),
              child: const Padding(
                padding: EdgeInsets.only(top: 150, left: 20, right: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Ready to learn',
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                        Text(
                          'TNPSC (Group 1)',
                          style: TextStyle(color: Colors.white, fontSize: 25),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),

            // group1
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 22, right: 22, top: 40),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Syllabus(prompt: prompt)),
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color:
                                      const Color(0xFF2F3247).withOpacity(0.1),
                                  border: Border.all(
                                      color: Colors.white, width: 0.2)),
                              width: 160,
                              height: 160,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(left: 20, top: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      height: 50,
                                      width: 50,
                                      decoration: BoxDecoration(
                                          color: Colors.white.withOpacity(0.1),
                                          borderRadius:
                                              BorderRadius.circular(50)),
                                      child: const Icon(
                                        Icons.stay_current_portrait,
                                        color: Colors.white,
                                      ),
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.only(top: 15),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Group 1',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 18),
                                          ),
                                          Text(
                                            'Syllabus',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 12),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),

                        // voice assist
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ChatCompletionPage(
                                        context1: "you are a tnpsc group1 exam guide"),
                                  ),
                                );
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: const Color(0xFF2F3247)
                                        .withOpacity(0.1),
                                    border: Border.all(
                                        color: Colors.white, width: 0.2)),
                                width: 160,
                                height: 160,
                                child: Padding(
                                  padding:
                                      const EdgeInsets.only(left: 20, top: 20),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 50,
                                        width: 50,
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.1),
                                            borderRadius:
                                                BorderRadius.circular(50)),
                                        child: const Icon(
                                          Icons.mic,
                                          color: Colors.white,
                                        ),
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.only(top: 15),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Group 1',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 18),
                                            ),
                                            Text(
                                              'Voice Assist',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 30, left: 22, right: 22),
                  child: Row(
                    children: [
                      Text(
                        'Mock test',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      )
                    ],
                  ),
                ),

                // test container
                Padding(
                    padding:
                        const EdgeInsets.only(left: 22, right: 22, top: 25),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => QuestionFetcher(
                                    prompt: prompt1,
                                    context: role,
                                  )),
                        );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: const Color(0xFF2F3247)
                                        .withOpacity(0.1),
                                    border: Border.all(
                                        color: Colors.white, width: 0.2)),
                                width: 160,
                                height: 160,
                                child: Padding(
                                  padding:
                                      const EdgeInsets.only(left: 20, top: 20),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 50,
                                        width: 50,
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.1),
                                            borderRadius:
                                                BorderRadius.circular(50)),
                                        child: const Icon(
                                          Icons.mic,
                                          color: Colors.white,
                                        ),
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.only(top: 15),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Group 1',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 18),
                                            ),
                                            Text(
                                              'General Science',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: const Color(0xFF2F3247)
                                        .withOpacity(0.1),
                                    border: Border.all(
                                        color: Colors.white, width: 0.2)),
                                width: 160,
                                height: 160,
                                child: Padding(
                                  padding:
                                      const EdgeInsets.only(left: 20, top: 20),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 50,
                                        width: 50,
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.1),
                                            borderRadius:
                                                BorderRadius.circular(50)),
                                        child: const Icon(
                                          Icons.mic,
                                          color: Colors.white,
                                        ),
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.only(top: 15),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Group 1',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 18),
                                            ),
                                            Text(
                                              'Aptitude and mental ability',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )),
                Row(
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 160, left: 22, right: 22),
                      child: Container(
                        width: 385,
                        height: 50,
                        color: const Color(0xFF64C3B2),
                        child: MaterialButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('Back'),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
