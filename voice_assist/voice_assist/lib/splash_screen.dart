import 'package:flutter/material.dart';
import 'package:voice_assist/auth/login_screen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset("assets/logo.png",
              width: 300, color: const Color.fromARGB(150, 255, 255, 255)),
          const Text('Explore with AI',
              style: TextStyle(
                  color: Color.fromARGB(150, 255, 255, 255), fontSize: 28)),
          const Text(
            'AI VOICE ASSIST',
            style: TextStyle(
                color: Color.fromARGB(150, 255, 255, 255),
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 15),
          const Text(
            'Explore courses designed to help you \n develop your skills and aid your \n knowledge',
            style: TextStyle(
                color: Color.fromARGB(150, 255, 255, 255), fontSize: 13),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 180,
          ),
          Container(
            width: 260,
            height: 50,
            color: const Color(0xFF64C3B2),
            child: MaterialButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0)),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const LoginScreen()));
              },
              child: const Text('start'),
            ),
          ),
        ],
      ),
    );
  }
}
