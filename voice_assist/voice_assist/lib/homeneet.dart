import 'package:flutter/material.dart';
import 'package:voice_assist/neet/Biology/biology_home.dart';
import 'package:voice_assist/neet/chemistry/chemistry_home.dart';
import 'package:voice_assist/neet/physics/physics_home.dart';

class Neethome extends StatefulWidget {
  const Neethome({super.key});

  @override
  State<Neethome> createState() => _NeethomeState();
}

class _NeethomeState extends State<Neethome> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 35, left: 22, right: 22),
          child: Row(
            children: [
              Text(
                'NEET',
                style: TextStyle(color: Colors.white, fontSize: 18),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 25, right: 25, top: 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Chemistryome()),
                      );
                    },
                    child: Container(
                      width: 171,
                      height: 62,
                      decoration: BoxDecoration(
                        color: const Color(0xFF2F3247).withOpacity(0.1),
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                          color: Colors.white,
                          width: 0.2,
                        ),
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Container(
                              width: 42,
                              height: 42,
                              decoration: BoxDecoration(
                                  gradient: const LinearGradient(
                                      colors: [
                                        Color(0xFF4CB6BD),
                                        Color(0xFF74FECC),
                                      ],
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight),
                                  borderRadius: BorderRadius.circular(5)),
                              child: const Icon(
                                Icons.mic,
                                color: Color.fromARGB(255, 3, 45, 80),
                              ),
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Text(
                              'Chemistry',
                              style: TextStyle(
                                  color: Color.fromARGB(255, 255, 255, 255)),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),

              //group2 container
              Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Physicshome()),
                      );
                    },
                    child: Container(
                      width: 171,
                      height: 62,
                      decoration: BoxDecoration(
                        color: const Color(0xFF2F3247).withOpacity(0.1),
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                          color: Colors.white,
                          width: 0.2,
                        ),
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Container(
                              width: 42,
                              height: 42,
                              decoration: BoxDecoration(
                                  gradient: const LinearGradient(
                                      colors: [
                                        Color(0xFF4CB6BD),
                                        Color(0xFF74FECC),
                                      ],
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight),
                                  borderRadius: BorderRadius.circular(5)),
                              child: const Icon(
                                Icons.mic,
                                color: Color.fromARGB(255, 3, 45, 80),
                              ),
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Text(
                              'Physics',
                              style: TextStyle(
                                  color: Color.fromARGB(255, 255, 255, 255)),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),

        //group3 container
        Padding(
          padding: const EdgeInsets.only(left: 25, right: 25, top: 18),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Biologyhome()),
                      );
                    },
                    child: Container(
                      width: 171,
                      height: 62,
                      decoration: BoxDecoration(
                        color: const Color(0xFF2F3247).withOpacity(0.1),
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                          color: Colors.white,
                          width: 0.2,
                        ),
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Container(
                              width: 42,
                              height: 42,
                              decoration: BoxDecoration(
                                  gradient: const LinearGradient(
                                      colors: [
                                        Color(0xFF4CB6BD),
                                        Color(0xFF74FECC),
                                      ],
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight),
                                  borderRadius: BorderRadius.circular(5)),
                              child: const Icon(
                                Icons.mic,
                                color: Color.fromARGB(255, 3, 45, 80),
                              ),
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Text(
                              'Biology',
                              style: TextStyle(
                                  color: Color.fromARGB(255, 255, 255, 255)),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
