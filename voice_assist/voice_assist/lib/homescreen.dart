import 'package:flutter/material.dart';
import 'package:voice_assist/constants/colors.dart';
import 'package:voice_assist/homeneet.dart';
import 'package:voice_assist/tnpsc/group1/group1_home.dart';
import 'package:voice_assist/tnpsc/group2/group2_home.dart';
import 'package:voice_assist/tnpsc/group3/group3_home.dart';
import 'package:voice_assist/tnpsc/group4/group4_home.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: const BoxDecoration(gradient: appGradient),
      child: Column(
        children: [
          Container(
            height: 250,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color(0xFF38BBD5),
                  Color(0xFF38BDCB),
                  Color(0xFF6EF5B5),
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: [0.0, 0.5, 1.0],
                tileMode: TileMode.clamp,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 150, left: 20, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Welcome Bose',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                      Text(
                        'Lets start learning',
                        style: TextStyle(color: Colors.white, fontSize: 25),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      const Padding(padding: EdgeInsets.only(top: 10)),
                      IconButton(onPressed: () {}, icon: const Icon(Icons.menu))
                    ],
                  )
                ],
              ),
            ),
          ),

          Column(
            children: [
              const Padding(
                padding: EdgeInsets.only(top: 40, left: 22, right: 22),
                child: Row(
                  children: [
                    Text(
                      'TNPSC',
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25, top: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Group1home()),
                            );
                          },
                          child: Container(
                            width: 171,
                            height: 62,
                            decoration: BoxDecoration(
                              color: const Color(0xFF2F3247).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(5.0),
                              border: Border.all(
                                color: Colors.white,
                                width: 0.2,
                              ),
                            ),
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Container(
                                    width: 42,
                                    height: 42,
                                    decoration: BoxDecoration(
                                        gradient: const LinearGradient(
                                            colors: [
                                              Color(0xFF4CB6BD),
                                              Color(0xFF74FECC),
                                            ],
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight),
                                        borderRadius: BorderRadius.circular(5)),
                                    child: const Icon(
                                      Icons.mic,
                                      color: Color.fromARGB(255, 3, 45, 80),
                                    ),
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.only(left: 20),
                                  child: Text(
                                    'Group 1',
                                    style: TextStyle(
                                        color:
                                            Color.fromARGB(255, 255, 255, 255)),
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),

                    //group2 container
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Group2home()),
                            );
                          },
                          child: Container(
                            width: 171,
                            height: 62,
                            decoration: BoxDecoration(
                              color: const Color(0xFF2F3247).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(5.0),
                              border: Border.all(
                                color: Colors.white,
                                width: 0.2,
                              ),
                            ),
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Container(
                                    width: 42,
                                    height: 42,
                                    decoration: BoxDecoration(
                                        gradient: const LinearGradient(
                                            colors: [
                                              Color(0xFF4CB6BD),
                                              Color(0xFF74FECC),
                                            ],
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight),
                                        borderRadius: BorderRadius.circular(5)),
                                    child: const Icon(
                                      Icons.mic,
                                      color: Color.fromARGB(255, 3, 45, 80),
                                    ),
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.only(left: 20),
                                  child: Text(
                                    'Group 2',
                                    style: TextStyle(
                                        color:
                                            Color.fromARGB(255, 255, 255, 255)),
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),

              //group3 container
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25, top: 18),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Group3home()),
                            );
                          },
                          child: Container(
                            width: 171,
                            height: 62,
                            decoration: BoxDecoration(
                              color: const Color(0xFF2F3247).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(5.0),
                              border: Border.all(
                                color: Colors.white,
                                width: 0.2,
                              ),
                            ),
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Container(
                                    width: 42,
                                    height: 42,
                                    decoration: BoxDecoration(
                                        gradient: const LinearGradient(
                                            colors: [
                                              Color(0xFF4CB6BD),
                                              Color(0xFF74FECC),
                                            ],
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight),
                                        borderRadius: BorderRadius.circular(5)),
                                    child: const Icon(
                                      Icons.mic,
                                      color: Color.fromARGB(255, 3, 45, 80),
                                    ),
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.only(left: 20),
                                  child: Text(
                                    'Group 3',
                                    style: TextStyle(
                                        color:
                                            Color.fromARGB(255, 255, 255, 255)),
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),

                    //group4 container
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Group4home()),
                            );
                          },
                          child: Container(
                            width: 171,
                            height: 62,
                            decoration: BoxDecoration(
                              color: const Color(0xFF2F3247).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(5.0),
                              border: Border.all(
                                color: Colors.white,
                                width: 0.2,
                              ),
                            ),
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Container(
                                    width: 42,
                                    height: 42,
                                    decoration: BoxDecoration(
                                        gradient: const LinearGradient(
                                            colors: [
                                              Color(0xFF4CB6BD),
                                              Color(0xFF74FECC),
                                            ],
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight),
                                        borderRadius: BorderRadius.circular(5)),
                                    child: const Icon(
                                      Icons.mic,
                                      color: Color.fromARGB(255, 3, 45, 80),
                                    ),
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.only(left: 20),
                                  child: Text(
                                    'Group 4',
                                    style: TextStyle(
                                        color:
                                            Color.fromARGB(255, 255, 255, 255)),
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),

          //neet module
          const Neethome()
        ],
      ),
    ));
  }
}
