import 'package:flutter/material.dart';

const LinearGradient appGradient = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFF191B28), Color(0xFF191B28)],
);
